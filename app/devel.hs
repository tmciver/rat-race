{-# LANGUAGE PackageImports #-}
import "ratrace" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
