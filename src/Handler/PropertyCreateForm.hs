{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Handler.PropertyCreateForm where

import Import hiding (PartialProperty)
import Ratrace.Domain.Property (PropertyUnit)
import Yesod.Form.Bootstrap3 (BootstrapFormLayout (..), renderBootstrap3)

getPropertyCreateFormR :: Handler Html
getPropertyCreateFormR = do
  (formWidget, formEnctype) <- generateFormPost propertyForm
  defaultLayout $ do
    setTitle "Ratrace: Create New Property"
    $(widgetFile "property-create-form")

data Address = Address { addrStreet :: Text
                       , addrCity :: Text
                       , addrZip :: Text
                       } deriving (Show, Eq)

data PartialProperty = PartialProperty
  { partialPropAddr :: Address
  , partialPropUnits :: [PropertyUnit]
  } deriving (Show, Eq)

-- propertyFromAddress :: Address -> IO Prop.Property
-- propertyFromAddress addr = pure $ Prop.Property (Prop.PropertyId 1) address
--   where address = Prop.USAddress (addrStreet addr) (addrCity addr) (addrZip addr)

createPartialProperty :: Address -> PartialProperty
createPartialProperty address = PartialProperty address []

propertyForm :: Form Address
propertyForm = renderBootstrap3 BootstrapBasicForm $ Address
  <$> areq textField "Street" Nothing
  <*> areq textField "City" Nothing
  <*> areq textField "Zip Code" Nothing
