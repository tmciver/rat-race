{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}

module Handler.Properties where

import Import hiding (undefined)
import Ratrace.Domain.Repository (save)
import Handler.PropertyCreateForm
  ( propertyForm
  , createPartialProperty
  , partialPropAddr
  , partialPropUnits
  , addrStreet
  , addrCity
  , addrZip
  )

getPropertiesR :: Handler Html
getPropertiesR = defaultLayout [whamlet|<h1>Properties!|]

postPropertiesR :: Handler Html
postPropertiesR = undefined
  -- do
  -- ((result, widget), enctype) <- runFormPost propertyForm
  -- case result of
  --   FormSuccess address -> do
  --     let property = createPartialProperty address
  --     (PPKey propId) <- save property
  --     liftIO $ Prelude.print property
  --     liftIO $ Prelude.putStrLn (show propId)
  --     defaultLayout $ do
  --       setTitle "Ratrace: Property"
  --       $(widgetFile "property")
  --   _ -> defaultLayout
  --        [whamlet|
  --                <p>Invalid input, let's try again.
  --                <form method=post action=@{PropertiesR} enctype=#{enctype}>
  --                  ^{widget}
  --                  <button>Submit
  --                  |]
