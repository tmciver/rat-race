{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Ratrace.Domain.App where

import Protolude

import Control.Monad.Trans.Reader (ReaderT)

data Env = Env

newtype AppM a = AppM (ReaderT Env IO a)
  deriving (Functor, Applicative, Monad)
