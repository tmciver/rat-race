{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Ratrace.Domain.Repository where

class Repository m e where
  type Key e
  getAll :: m [e]
  getById :: Key e -> m (Maybe e)
  save :: e -> m (Key e)
