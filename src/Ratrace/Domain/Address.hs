{-# LANGUAGE NoImplicitPrelude #-}

module Ratrace.Domain.Address where

import Protolude

data Address = Address { addrStreet :: Text
                       , addrCity :: Text
                       , addrZip :: Text
                       } deriving (Show, Eq)
