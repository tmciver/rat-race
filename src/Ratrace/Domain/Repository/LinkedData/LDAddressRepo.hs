{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Ratrace.Domain.Repository.LinkedData.LDAddressRepo where

import Protolude

import Data.Maybe (fromJust)
import Network.URL (URL, importURL)
import Ratrace.Domain (Address)
import Ratrace.Domain.App (AppM)
import Ratrace.Domain.Repository

instance Repository AppM Address where
  type Key Address = URL

  getAll = pure []
  getById uri = pure Nothing
  save _ = pure . fromJust . importURL $ "http://example.com/address1"
