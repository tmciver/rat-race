module Ratrace.Domain.Property where

import Data.Text (Text)
import Data.List.NonEmpty (NonEmpty)

newtype PropertyId = PropertyId Int deriving (Eq, Show)
data Address = USAddress { addrStreet :: Text
                         , addrCity :: Text
                         , addrZip :: Text
                         } deriving (Eq, Show)

data Property = Property { propertyId :: PropertyId
                         , propertyAddr :: Address
                         , propertyUnits :: NonEmpty PropertyUnit
                         } deriving (Eq, Show)

data PropertyUnit = PropertyUnit
  { unitNumBaths :: Int
  , unitNumBedrooms :: Int
  } deriving (Eq, Show)
